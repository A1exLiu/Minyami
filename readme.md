# README

## Dependencies

* openssl
* mkvmerge

Make sure you had put these binary files into your system `PATH`.

## Installation

`npm install minyami -g`

There's also a UserScript in the root directory of this project called `Minyami Extractor` which can help you to extract m3u8 files urls from web pages. Please install it with a browser UserScript management extension such as VioletMoney(recommend).

Press `Enter` twice to show `Minyami Extractor` in support websites.

## Contribution

Minyami is developed with TypeScript. You need to install TypeScript Compiler before you start coding.

**Install development dependencies**

```
npm install -g typescript
git clone https://github.com/Last-Order/Minyami
cd Minyami
npm install
```

To build the project, just run `tsc`.

## Copyright

Open-sourced under GPLv3. © 2018, Eridanus Sora, member of MeowSound Idols.