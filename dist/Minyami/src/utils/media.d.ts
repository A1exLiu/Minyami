/**
 * 合并视频文件
 * @param fileList 文件列表
 * @param output 输出路径
 */
export declare function mergeVideo(fileList?: any[], output?: string): Promise<void>;
