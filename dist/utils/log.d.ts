declare class Log {
    static debug(message: string): void;
    static info(message: string): void;
    static warning(message: string): void;
    static error(message: string): void;
}
export default Log;
