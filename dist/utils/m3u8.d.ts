import M3U8 from "../core/m3u8";
export declare function loadM3U8(path: string, retries?: number, timeout?: number): Promise<M3U8>;
